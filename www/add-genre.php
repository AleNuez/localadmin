<?php include 'head.php'; ?>
<?php include 'navbar.php'; ?>
<?php include 'config.php'; ?>
<?php
//1. Definir variables y inicializarlas vacías.
$genre = "";
$genre_err = "";
 
//2. Proceso cuando se submitea
if(isset($_POST["genre-nombre"])){
    // Validar si existe
    if(empty(trim($_POST["genre-nombre"]))){
        $genre_err = "Por favor ingrese un género.";
    } else{
        // Prepare a select statement
        $sql = "SELECT genre FROM genre WHERE genre = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_genre);
            
            // Set parameters
            $param_genre = trim($_POST["genre-nombre"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) == 1){
                    $genre_err = "Este género ya existe.";
                } else{
                    $genre = trim($_POST["genre-nombre"]);
                }
            } else{
                echo "Al parecer algo salió mal.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }  
    // Check input errors before inserting in database
    if(empty($genre_err)){
        
        // Prepare an insert statement
        $sql = "INSERT INTO genre (genre) VALUES (?)";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_genre);
            
            // Set parameters
            $param_genre = trim($_POST["genre-nombre"]) ;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Redirect to login page
                ?>
                 <script> location.replace("genres.php"); </script>
                <?php
               // header("location: genres.php");
            } else{
                echo "Algo salió mal, por favor inténtalo de nuevo.";
            }
        }
         //echo var_dump($stmt);
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
}
?>
<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Agregar un género musical al catálogo</h1>
<!-- Content Row -->
<div class="row">
<div class=" col-12 card shadow mb-4">
        <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Agregar género</h6>
        </div>    
        <div class="card-body">
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>"method="post" class="user">
                                        <div class="form-group row <?php echo (!empty($genre_err)) ? 'has-error' : ''; ?>">
                                        <!-- NOMBRE DEL GÉNERO -->
                                        <div class="col-12">
                                        <input type="text" name="genre-nombre" class="form-control form-control-user"
                                                id="genre-nombre" aria-describedby="genre-nombre"
                                                placeholder="Nombre" required>
                                                <span class="help-block text-danger"><?php echo $genre_err; ?></span>
                                        </div>    
                                       
                                        </div>

                                        <div class="form-group">
                                         <input type="submit" class="btn btn-primary" value="Añadir">
                                         <a href="genres.php" class="btn btn-danger">Cancelar</a>
                                        </div>
                                        <!-- <a href="main.php" class="btn btn-primary btn-user btn-block">
                                            Login
                                        </a> -->
                                       
                                    </form>

        </div>
<!-- Content Row -->
<?php include 'footer.php'; ?>