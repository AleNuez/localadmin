<?php include 'head.php'; ?>
<?php include 'navbar.php'; ?>
<?php include 'config.php'; ?>

    <!-- Hero -->
<div class="row">
<div class="col-12 bg-hero"></div>
</div>
            <!-- Page Heading -->
            <br>
            <h1 class="h3 mb-4 text-gray-800">¡Te damos la bienvenida <b><?php echo ucfirst($_SESSION["name"]); ?></b>!</h1>
        
   
            <!-- Tarjetas Row -->
            <div class="row">

<!--Articulos Card Example -->
<div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-warning shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Artículos
                                    </div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">
                                    <?php 
                                    $sql_articles = "SELECT COUNT(article_id) as articulos FROM articles;";
                                    if($result_busq_articles = mysqli_query($link, $sql_articles)){
                                        if(mysqli_num_rows($result_busq_articles) > 0){
                                            while($row_busq_articles = mysqli_fetch_array($result_busq_articles)){
                                               echo "". $row_busq_articles['articulos'].""; }}}
                                    ?>  
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-archive fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Albumes Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Albumes</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">
                                    <?php 
                                    $sql_albumes = "SELECT COUNT(id) as albumes FROM album;";
                                    if($result_busq_alb = mysqli_query($link, $sql_albumes)){
                                        if(mysqli_num_rows($result_busq_alb) > 0){
                                            while($row_busq_alb = mysqli_fetch_array($result_busq_alb)){
                                               echo "". $row_busq_alb['albumes'].""; }}}
                                    ?>    
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-music fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Artistas Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-success shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                        Artistas</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">
                                    <?php 
                                    $sql_art = "SELECT COUNT(id) as artistas FROM artist;";
                                    if($result_busq_art = mysqli_query($link, $sql_art)){
                                        if(mysqli_num_rows($result_busq_art) > 0){
                                            while($row_busq_art = mysqli_fetch_array($result_busq_art)){
                                               echo "". $row_busq_art['artistas'].""; }}}
                                    ?>    
                                </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fa fa-users fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Generos Card Example -->
                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-info shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Géneros
                                    </div>
                                    <div class="row no-gutters align-items-center">
                                        <div class="col-auto">
                                            <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                                            <?php 
                                    $sql_gen = "SELECT COUNT(id) as generos FROM genre;";
                                    if($result_busq_gen = mysqli_query($link, $sql_gen)){
                                        if(mysqli_num_rows($result_busq_gen) > 0){
                                            while($row_busq_gen = mysqli_fetch_array($result_busq_gen)){
                                               echo "". $row_busq_gen['generos'].""; }}}
                                    ?>     
                                            </div>
                                        </div>
                                        <!-- <div class="col"> <div class="progress progress-sm mr-2"> <div
                                        class="progress-bar bg-info" role="progressbar" style="width: 50%"
                                        aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div> </div> </div>
                                        -->
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-tags fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
            <!-- /Tarjetas Row -->

            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Ultimas Adiciones</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                    <?php  $sql = "SELECT audits.id as ID, audits.date as Fecha, audits.action as Action, historic_names.name as Album, artist.name AS Artista, formats.format as Formato FROM audits INNER JOIN historic_names ON audits.article = historic_names.id INNER JOIN formats ON audits.format = formats.id INNER join album on historic_names.name = album.name INNER JOIN artist ON artist.id = album.artist_id WHERE Action LIKE '%agregó%'"; ?>
                    <table class="table table-bordered" id="tabla" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                        <th>ID</th>
                                        <th>Fecha</th>
                                        <th>Disco</th>
                                        <th>Artista</th>
                                        <th>Formato</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php   if($result_busq = mysqli_query($link, $sql)){
                        if(mysqli_num_rows($result_busq) > 0){
                            while($row_busq = mysqli_fetch_array($result_busq)){
                                echo "<tr>";
                                    echo "<td>" . $row_busq['ID'] . "</td>";
                                    echo "<td>" . $row_busq['Fecha'] . "</td>";
                                    echo "<td>" . $row_busq['Album'] . "</td>";
                                    echo "<td>" . $row_busq['Artista'] . "</td>";
                                    echo "<td>" . $row_busq['Formato'] . "</td>";
                                echo "</tr>";}}}
                                ?>
                                      
                                    </tbody>
                    </table>
                    </div>
                </div>
            </div>
             <!-- /DataTales Example -->

            <?php include 'footer.php'; ?>