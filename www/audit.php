<?php include 'head.php'; ?>
<?php include 'navbar.php'; ?>
<?php include 'config.php'; ?>

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Auditoria</h1>
<!-- Content Row -->
<div class="row">

    <!-- Content Row -->
    <!-- DataTales Example -->
    <div class=" col-12 card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Ultimos Movimientos</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <?php  $sql = "SELECT audits.id as ID, audits.date as Fecha, audits.action as Action, historic_names.name as Album, artist.name AS Artista, formats.format as Formato FROM audits INNER JOIN historic_names ON audits.article = historic_names.id INNER JOIN formats ON audits.format = formats.id INNER join album on historic_names.name = album.name INNER JOIN artist ON artist.id = album.artist_id"; ?>
                <table class="table table-bordered" id="tabla" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Fecha</th>
                            <th>Suceso</th>
                            <th>Disco</th>
                            <th>Artista</th>
                            <th>Formato</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody>
                    <?php   if($result_busq = mysqli_query($link, $sql)){
                        if(mysqli_num_rows($result_busq) > 0){
                            while($row_busq = mysqli_fetch_array($result_busq)){
                                echo "<tr>";
                                    echo "<td>" . $row_busq['ID'] . "</td>";
                                    echo "<td>" . $row_busq['Fecha'] . "</td>";
                                    echo "<td>" . $row_busq['Action'] . "</td>";
                                    echo "<td>" . $row_busq['Album'] . "</td>";
                                    echo "<td>" . $row_busq['Artista'] . "</td>";
                                    echo "<td>" . $row_busq['Formato'] . "</td>";
                                echo "</tr>";}}}
                                ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- </div> -->
    <!--/ DataTales Example -->
  
    <?php include 'footer.php'; ?>