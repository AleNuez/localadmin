<?php include 'head.php'; ?>
<?php include 'navbar.php'; ?>
<?php include 'config.php'; ?>
<?php session_start(); ?>
<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Catálogo</h1>
<!-- Content Row -->
<div class="row">

    <!-- Content Row -->
    <!-- DataTables -->
    <div class=" col-12 card shadow mb-4">
        <div class="card-header py-3">
        <div class="row">
            <div class="col-9">  <h6 class="m-0 font-weight-bold text-primary">Albumes | La Música de Gogo</h6> </div>
       <?php if($_SESSION["rol_id"] == "1") { ?>
  <form action="add-album.php" method="post">
  <div class="col-3"> <input type="submit" class="btn btn-primary" value=" + | Agregar Album"></div>
  </form>
    <?php } ?>
            </div>     
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <?php  $sql = "SELECT article_id as ID, album.name as Album, artist.name as Artista, year as Año, format as Formato, price as Precio FROM articles INNER JOIN album ON articles.album_id = album.id INNER JOIN artist on album.artist_id = artist.id INNER JOIN formats ON articles.format_id = formats.id;"; ?>
                <table class="table table-bordered" id="tabla" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Artista</th>
                            <th>Año</th>
                            <th>Formato</th>
                            <th>Precio</th>
                            <?php if($_SESSION["rol_id"] == "1") { ?><th>Acciones</th>  <?php } ?>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody>
                 <?php   if($result_busq = mysqli_query($link, $sql)){
                        if(mysqli_num_rows($result_busq) > 0){
                            while($row_busq = mysqli_fetch_array($result_busq)){
                                echo "<tr>";
                                    echo "<td>" . $row_busq['ID'] . "</td>";
                                    echo "<td>" . $row_busq['Album'] . "</td>";
                                    echo "<td>" . $row_busq['Artista'] . "</td>";
                                    echo "<td>" . $row_busq['Año'] . "</td>";
                                    echo "<td>" . $row_busq['Formato'] . "</td>";
                                    echo "<td>" . $row_busq['Precio'] . "</td>";
                                    if($_SESSION["rol_id"] == "1") { 
                                    echo "<td>";
                                    // echo '<a href="read.php?id='.$row_busq2['genre_id'] .'" class="mr-3" title="Ver" data-toggle="tooltip"><span class="fa fa-eye"></span></a>';
                                    // Si hay albumes con generos guardados imposibilito el poder eliminar el género, solo se pueden borrar los generos sin albumes relacionados
                                    //$disabled_click = ($disponibles>0) ? "style=\"pointer-events:none\";" : ""; 
                                    //$disabled_color = ($disponibles>0) ? "text-secondary" : "text-danger"; 
                                    echo '<a href="update-album.php?id='.$row_busq['ID'] .'" class="mr-3" title="Editar" data-toggle="tooltip"><span class="fa fa-edit text-success"></span></a>';
                                     echo '<a href="delete-album.php?id='.$row_busq['ID'] .'" title="Eliminar" data-toggle="tooltip"><span class="fa fa-trash text-danger"></span></a>';
                                     echo "</td>";
                                    }
                                echo "</tr>";
                            }
                        }
                        }
                            ?>

                        <!-- <tr>
                            <td>A001</td>
                            <td>Toxicity</td>
                            <td>System Of a Down</td>
                            <td>2001</td>
                            <td>CD</td>
                            <td>$3000</td>
                            <td>Ver Más</td>
                        </tr> -->

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- </div> -->
    <!--/ DataTables -->

    <?php include 'footer.php'; ?>