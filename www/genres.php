<?php include 'head.php'; ?>
<?php include 'navbar.php'; ?>
<?php include 'config.php'; ?>
<?php session_start(); ?>
<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Géneros</h1>
<!-- Content Row -->
<div class="row">

    <!-- Content Row -->
    <!-- DataTales Example -->
    <div class="col-7 card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col-7">
                    <h6 class="m-0 font-weight-bold text-primary">Géneros Músicales | La Música de Gogo</h6>
                </div>
                <?php if($_SESSION["rol_id"] == "1") { ?>
                <form action="add-genre.php" method="post">
                    <div class="col-5">
                        <input type="submit" class="btn btn-primary" value=" + | Agregar Género"></div>
                </form>
                <?php } ?>
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <?php  $sql = "SELECT id as ID, genre as Género FROM genre;";?>
                <table class="table table-bordered" id="tabla" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Género</th>
                            <th>Artistas Disponibles</th>
                            <?php if($_SESSION["rol_id"] == "1") { ?><th>Acciones</th>  <?php } ?>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php   if($result_busq = mysqli_query($link, $sql)){
                        if(mysqli_num_rows($result_busq) > 0){
                            while($row_busq = mysqli_fetch_array($result_busq)){
                                echo "<tr>";
                                    echo "<td>" . $row_busq['ID'] . "</td>";
                                    echo "<td>" . $row_busq['Género'] . "</td>";
                                    $sql2 = "SELECT COUNT(genre_id) as Disponible FROM artist WHERE genre_id ={$row_busq['ID']};";
                                    if($result_busq2 = mysqli_query($link, $sql2)){
                                          if(mysqli_num_rows($result_busq2) > 0){
                                              while($row_busq2 = mysqli_fetch_array($result_busq2)){
                                                $disponible[] = $row_busq2['Disponible'];
                                                $disponibles = $row_busq2['Disponible'];
                                                 echo "<td>". $row_busq2['Disponible']."</td>"; }}}
                                                 if($_SESSION["rol_id"] == "1") { 
                                                 echo "<td>";
                                   // echo '<a href="read.php?id='.$row_busq2['genre_id'] .'" class="mr-3" title="Ver" data-toggle="tooltip"><span class="fa fa-eye"></span></a>';
                                   // Si hay albumes con generos guardados imposibilito el poder eliminar el género, solo se pueden borrar los generos sin albumes relacionados
                                   $disabled_click = ($disponibles>0) ? "style=\"pointer-events:none\";" : ""; 
                                   $disabled_color = ($disponibles>0) ? "text-secondary" : "text-danger"; 
                                   echo '<a href="update-genre.php?id='.$row_busq['ID'] .'" class="mr-3" title="Editar" data-toggle="tooltip"><span class="fa fa-edit text-success"></span></a>';
                                    echo '<a '.$disabled_click.' href="delete-genre.php?id='.$row_busq['ID'] .'" title="Eliminar" data-toggle="tooltip"><span class="fa fa-trash '.$disabled_color.'"></span></a>';
                                    echo "</td>";
                                                 }
                                echo "</tr>";
                            }
                        }
                        }
                            ?>
                        <!-- <tr> <td>G001</td> <td>Metal</td> <td>5</td> <td>Explorar Género</td> </tr>
                        -->

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- </div> -->
    <!--/ DataTales Example -->
    <!-- pie chart -->
    <div class="col-xl-4 col-lg-6">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Géneros disponibles</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-pie pt-4">
                    <canvas id="myPieChart"></canvas>
                </div>

            </div>
        </div>
    </div>
    <!-- /pie chart -->
            <!-- </div> -->
            <!--/ DataTales Example -->
            </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->

    <!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Alejandro Nuñez &copy; LocalAdmin V1.0</span>
            </div>
        </div>
    </footer>
    <!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
<i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div
class="modal fade"
id="logoutModal"
tabindex="-1"
role="dialog"
aria-labelledby="exampleModalLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Realmente desea salir?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">Si se desloguea, se cerrará su sesión.</div>
        <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            <a class="btn btn-primary" href="logout.php">Salir</a>
        </div>
    </div>
</div>
</div>
<?php
$sqlg = "SELECT * FROM genre;";
$result_busqg = mysqli_query($link, $sqlg);
foreach ($result_busqg as $data){
    $generos[] = $data['genre'];
}
foreach ($generos as $numeros){
    $cantidad[] = 1;
}
//echo var_dump($generos);
//echo var_dump($cantidad);
//echo var_dump($disponible);
?>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Page level plugins -->
<script src="vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="js/demo/chart-area-demo.js"></script>
<script>
    // Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

// Pie Chart Example
var ctx = document.getElementById("myPieChart");
var myPieChart = new Chart(ctx, {
  type: 'doughnut',
  data: {
    labels: <?php echo json_encode($generos); ?>,
    datasets: [{
      data: <?php echo json_encode($disponible); ?>,
       backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc','#28C0BE','#FD3B71','#FBE05D','#D9741F','#FA5EF6','#C2F9B5','#F35115','#1DF006','#487C63'],
      hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf'],
      hoverBorderColor: "rgba(234, 236, 244, 1)",
    }],
  },
  options: {
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
    },
    legend: {
      display: false
    },
    cutoutPercentage: 80,
  },
});

</script>
<script src="js/demo/chart-bar-demo.js"></script>
<!-- Custom scripts for all pages-->
<script src="js/sb-admin-2.min.js"></script>´
<script>

var myTable = document.querySelector("#tabla");
var dataTable = new DataTable(myTable);
</script>

</body>

</html>