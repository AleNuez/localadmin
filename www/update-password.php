<?php
// Esta página toma el resultado del form de actualizar contraseña
// Lo hice aparte porque en el self de change-password valido con el post si se recibió el email.

session_start();
// Include config file
require_once "config.php";


// Define variables and initialize with empty values
$password = $confirm_password = "";
$$password_err = $confirm_password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST['password'] != NULL){
 

    // Validate password

    if(empty(trim($_POST["password"]))){
        $password_err = "Por favor ingresa una contraseña.";     
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "La contraseña al menos debe tener 6 caracteres.";
    } else{
        $password = trim($_POST["password"]);
    }
    
    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Confirma tu contraseña.";     
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "No coincide la contraseña.";
        }
    }
    $password = $_POST["password"];
    // Check input errors before inserting in database
          //$param_password = password_hash($password, PASSWORD_DEFAULT);
        // Prepare an insert statement
        $sql ="UPDATE users SET password = ? WHERE email = ?";        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ss", $param_pas, $param_email);
            // Set parameters
            $param_pas = password_hash($password, PASSWORD_DEFAULT); 
            $param_email = $_SESSION['email-to-restore'];
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Redirect to login page
           
              header("location: login.php");
            } else{
                echo "Algo salió mal, por favor inténtalo de nuevo.";
            }
        }
        // Close statement
        mysqli_stmt_close($stmt);
    
    // Close connection
    mysqli_close($link);
}
?>