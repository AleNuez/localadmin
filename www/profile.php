<?php
// // Include config file
// require_once "config.php";
 
// // Define variables and initialize with empty values
// $username = $password = $confirm_password = "";
// $username_err = $password_err = $confirm_password_err = "";
 
// // Processing form data when form is submitted
// if($_SERVER["REQUEST_METHOD"] == "POST"){
 
//     // Validate username
//     if(empty(trim($_POST["username"]))){
//         $username_err = "Por favor ingrese un usuario.";
//     } else{
//         // Prepare a select statement
//         $sql = "SELECT id FROM users WHERE username = ?";
        
//         if($stmt = mysqli_prepare($link, $sql)){
//             // Bind variables to the prepared statement as parameters
//             mysqli_stmt_bind_param($stmt, "s", $param_username);
            
//             // Set parameters
//             $param_username = trim($_POST["username"]);
            
//             // Attempt to execute the prepared statement
//             if(mysqli_stmt_execute($stmt)){
//                 /* store result */
//                 mysqli_stmt_store_result($stmt);
                
//                 if(mysqli_stmt_num_rows($stmt) == 1){
//                     $username_err = "Este usuario ya fue tomado.";
//                 } else{
//                     $username = trim($_POST["username"]);
//                 }
//             } else{
//                 echo "Al parecer algo salió mal.";
//             }
//         }
         
//         // Close statement
//         mysqli_stmt_close($stmt);
//     }
    
//     // Validate password
//     if(empty(trim($_POST["password"]))){
//         $password_err = "Por favor ingresa una contraseña.";     
//     } elseif(strlen(trim($_POST["password"])) < 6){
//         $password_err = "La contraseña al menos debe tener 6 caracteres.";
//     } else{
//         $password = trim($_POST["password"]);
//     }
    
//     // Validate confirm password
//     if(empty(trim($_POST["confirm_password"]))){
//         $confirm_password_err = "Confirma tu contraseña.";     
//     } else{
//         $confirm_password = trim($_POST["confirm_password"]);
//         if(empty($password_err) && ($password != $confirm_password)){
//             $confirm_password_err = "No coincide la contraseña.";
//         }
//     }
    
//     // Check input errors before inserting in database
//     if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){
        
//         // Prepare an insert statement
//         $sql = "INSERT INTO users (username, password) VALUES (?, ?)";
         
//         if($stmt = mysqli_prepare($link, $sql)){
//             // Bind variables to the prepared statement as parameters
//             mysqli_stmt_bind_param($stmt, "ss", $param_username, $param_password);
            
//             // Set parameters
//             $param_username = $username;
//             $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash
            
//             // Attempt to execute the prepared statement
//             if(mysqli_stmt_execute($stmt)){
//                 // Redirect to login page
//                 header("location: login.php");
//             } else{
//                 echo "Algo salió mal, por favor inténtalo de nuevo.";
//             }
//         }
         
//         // Close statement
//         mysqli_stmt_close($stmt);
//     }
    
//     // Close connection
//     mysqli_close($link);
// }
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>SB Admin 2 - Register</title>

        <!-- Custom fonts for this template-->
        <link
            href="vendor/fontawesome-free/css/all.min.css"
            rel="stylesheet"
            type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
    </head>

    <body class="bg-gradient-primary">

        <div class="container">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-5 d-none d-lg-block bg-profile-bg">
<div class="row">
<div class="col-2"></div>    
                        <div class="col-8 mt-5 bg-profile-imagen"></div>
                        <div class="col-2"></div>   
</div>
                         
                    </div>
                        <div class="col-lg-7">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Tu Perfil</h1>
                                </div>
                                <form
                                    class="user"
                                    action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>"
                                    method="post">
                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <input
                                                type="text"
                                                class="form-control form-control-user"
                                                id="exampleFirstName"
                                                placeholder="First Name">
                                        </div>
                                        <div class="col-sm-6">
                                            <input
                                                type="text"
                                                class="form-control form-control-user"
                                                id="exampleLastName"
                                                placeholder="Last Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input
                                            type="email"
                                            class="form-control form-control-user"
                                            id="exampleInputEmail"
                                            placeholder="Email Address">
                                    </div>
                                    <a href="profile.php" class="btn btn-primary btn-user btn-block">
                                        Actualizar Datos
                                    </a>
                                    <hr>
                                    <div class="form-group row">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <input
                                                type="password"
                                                class="form-control form-control-user"
                                                id="exampleInputPassword"
                                                placeholder="Password">
                                        </div>
                                        <div class="col-sm-6">
                                            <input
                                                type="password"
                                                class="form-control form-control-user"
                                                id="exampleRepeatPassword"
                                                placeholder="Repeat Password">
                                        </div>
                                    </div>
                                    <hr>
                                    <a href="profile.php" class="btn btn-primary btn-user btn-block">
                                        Cambiar Contraseña
                                    </a>
                                    <hr>
                                    <a href="profile.php" class="btn btn-primary btn-user btn-block">
                                        Actualizar Foto
                                    </a>

                                </form>
                                <a href="main.php" class="mt-4 btn btn-danger btn-user float-right">
                                    Volver
                                </a>
                                <hr>
                                <!-- <div class="text-center"> <a class="small"
                                href="forgot-password.html">Forgot Password?</a> </div> <div
                                class="text-center"> <a class="small" href="login.html">Already have an account?
                                Login!</a> </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>

    </body>

</html>