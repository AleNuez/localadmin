<?php include 'head.php'; ?>
<?php include 'navbar.php'; ?>
<?php include 'config.php'; ?>
<?php

//echo var_dump($_POST);
//0. Proceso la imagen
 $album_img = "";

 //1. Definir variables y inicializarlas vacías.
 $album_exists = false;
 $album_loaded = false;
 $album_nombre = $album_artista = $album_lanzamiento = $album_genero = $album_stock = $album_formato = $album_precio = "";
 $album_nombre_err = $album_artista_err = $album_lanzamiento_err = $album_genero_err = $album_stock_err = $album_formato_err = $album_precio_err = $format_ok_err = "";
 
//2. Chequeo si ya existe la entrada.
if(isset($_POST["album-nombre"])){
    // Validar si existe
    if(empty(trim($_POST["album-nombre"]))){
        $album_nombre_err = "Por favor ingrese un nombre.";
    } else{
        // Prepare a select statement
        $sql = "SELECT id, name FROM album WHERE name = ?";
        
            if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_name);
            // Set parameters
            $param_name = ucwords(trim($_POST["album-nombre"]));
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
            /* store result */
            mysqli_stmt_store_result($stmt);
            // This is for taking the id
                $psql = "SELECT id, name FROM album WHERE name = '$param_name'";
                if($result = mysqli_query($link, $psql)){
                    if(mysqli_num_rows($result) > 0){
                    while($row = mysqli_fetch_array($result)){$sid = $row['id'];}
                    } else{
                    //echo 'Todavia no existe ese album.';
                    }
                } else { echo "Fatal param."; }
            // tengo que cerrar 2 llaves hasta acá
 
                if(mysqli_stmt_num_rows($stmt) == 1){
                    //$artist_name_err = "Este album ya existe.";
                    //echo $artist_name_err;
                    //echo $sid;   
                // AHORA A FIJARME EL FORMATO
                   mysqli_stmt_close($stmt);
                   // Prepare a select statement
                   $sformat = $_POST["album-formato"];
                   $ssql = "SELECT * FROM articles WHERE album_id = '$sid' and format_id = '$sformat' ";
                   if($result = mysqli_query($link, $ssql)){
                   if(mysqli_num_rows($result) > 0){
                        $format_ok_err = "Este album con este formato ya existe. Si desea agregar ejemplares de un album existente edite desde el catálogo.";
                   } else{
                    $album_exists = true;
                    //echo 'No hay formato repetido';
                    }
                    } else { echo "Fatal param."; }
                // /AHORA A FIJARME EL FORMATO
                } else{
                    $album_name = ucwords(trim($_POST["album-nombre"]));
                }
            } else{
                echo "Al parecer algo salió mal.";
            }
        }
         
        // Close statement
        //mysqli_stmt_close($stmt);
    }  
//3. Check input errors before inserting in database
    if(empty($album_nombre_err) && empty($format_ok_err) ){
        if($album_exists){
        //echo "carga en article"; SI EXISTE EL ALBUM, SOLO CARGO DATOS EN ARTICLES
        // Prepare an insert statement
        $sql = "INSERT INTO articles (album_id, format_id, price) VALUES (?,?,?)";
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "sss", $param_album_id, $param_format_id, $param_price);
            // Set parameters
            $param_album_id = $sid;
            $param_format_id = $sformat;
            $param_price = $_POST["album-precio"];
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Redirect to login page
                
            ?>
            <script>
            location.replace("albums.php");
            </script>
            <?php
               // header("location: genres.php");
            } else{
                //echo "Algo salió mal, por favor inténtalo de nuevo.";
            }
        }
         //echo var_dump($stmt);
        // Close statement
        mysqli_stmt_close($stmt);
        
        } else {
        //echo "carga en article y album"; SI NO EXISTE, CREO ALBUM EN ALBUMS Y ADEMAS ARTICLE.
       // Prepare an insert statement
       $sql = "INSERT INTO album (name, artist_id, genre_id, cover, year, publisher_id, details) VALUES (?,?,?,?,?,?,?)";  
       if($stmt = mysqli_prepare($link, $sql)){
           // Bind variables to the prepared statement as parameters
           mysqli_stmt_bind_param($stmt, "sssssss", $param_name, $param_artist_id, $param_genre_id, $param_cover, $param_year, $param_publisher_id, $param_details);
           // Set parameters
           $param_name = $_POST["album-nombre"];
           $param_artist_id = $_POST["album-artista"];
           $param_genre_id = "";
           $param_cover = "https://f4.bcbits.com/img/a4139357031_10.jpg" ;
           $param_year = $_POST["album-lanzamiento"];
           $param_publisher_id = "" ;
           $param_details = "" ;
           // Attempt to execute the prepared statement
           if(mysqli_stmt_execute($stmt)){
               // Redirect to login page
            $album_loaded = true;
              // header("location: genres.php");
           } else{
               //echo "Algo salió mal, por favor inténtalo de nuevo.";
           }
           mysqli_stmt_close($stmt);
       }
        //echo var_dump($stmt);
       // Close statement
     }

     if($album_loaded){
    //tengo que buscar el id del album recién creado
        // Prepare a select statement
            // $sqid = "SELECT id, name FROM album WHERE name = ?";
            // if($stmt = mysqli_prepare($link, $sqlid)){
            // // Bind variables to the prepared statement as parameters
            // mysqli_stmt_bind_param($stmt, "s", $param_name);
            // // Set parameters
            // $param_name = ucwords(trim($_POST["album-nombre"]));
            // // Attempt to execute the prepared statement
            // if(mysqli_stmt_execute($stmt)){
            // /* store result */
            // mysqli_stmt_store_result($stmt);
            // This is for taking the id
                $psqlid = "SELECT id, name FROM album WHERE name = '$param_name'";
                if($result = mysqli_query($link, $psqlid)){
                    if(mysqli_num_rows($result) > 0){
                    while($row = mysqli_fetch_array($result)){
                        $sid = $row['id'];}
                    } else{
                    //echo 'Todavia no existe ese album.';
                    }
                } else { echo "Fatal param."; }
            // tengo que cerrar 2 llaves hasta acá
                 // aca meto el article
    // Prepare an insert statement
    $sqlb = "INSERT INTO articles (album_id, format_id, price) VALUES (?,?,?)";
    if($stmt = mysqli_prepare($link, $sqlb)){
    mysqli_stmt_bind_param($stmt, "sss", $param_album_id, $param_format_id, $param_price);
    // Set parameters
    $param_album_id = $sid;
    $param_format_id = $_POST["album-formato"];
    $param_price = $_POST["album-precio"];
    // Attempt to execute the prepared statement
    if(mysqli_stmt_execute($stmt)){
        // Redirect to login page
    ?>
    <script>
    location.replace("albums.php");
    </script>
    <?php
       // header("location: genres.php");
    } else{
        //echo "Algo salió mal, por favor inténtalo de nuevo.";
    }
}
 //echo var_dump($stmt);
// Close statement
mysqli_stmt_close($stmt);

// /article
     }
    } 
    // Close connection
    mysqli_close($link);
 }
 
?>

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Agregar disco al catálogo</h1>
<!-- Content Row -->
<div class="row">
    <div class=" col-12 card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Agregar album</h6>
        </div>
        <div class="card-body">
            <form
                action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>"
                method="post"
                class="user"
                enctype="multipart/form-data">
                <div
                    class="form-group row <?php echo (!empty($album_nombre_err)) ? 'has-error' : ''; ?>">
                    <!-- NOMBRE DE ALBUM -->
                    <div class="col-6">
                        <input
                            type="text"
                            name="album-nombre"
                            class="form-control form-control-user-custom"
                            id="album-nombre"
                            aria-describedby="album-nombre"
                            placeholder="Nombre"
                            required="required">
                        <span class="help-block text-danger"><?php echo $album_nombre_err; ?></span>
                    </div>
                    <!-- ARTISTA -->
                    <div class="col-6">
                        <select
                            name="album-artista"
                            class="form-control form-control-user-custom"
                            required="required">
                            <option value="Elegir">Elegir Artista</option>
                        <?php // Cargar el primer Select
                                        $sql = "SELECT * FROM artist";
                                         if($result = mysqli_query($link, $sql)){
                                         if(mysqli_num_rows($result) > 0){
                                         while($row = mysqli_fetch_array($result)){
                                                 echo "<option value=".$row['id'].">".$row['name']."</option>";
                                         }
                                         } else{
                                          echo '<option>No hay artistas</option>';
                                          }
                                         } else { echo "Fatal Select."; }
                                        ?>
                        </select>
                        <span class="help-block text-danger"><?php echo $album_artista_err; ?></span>
                    </div>
                </div>
                <div
                    class="form-group row <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                    <!-- AÑO DE LANZAMIENTO -->
                    <div class="col-6">
                        <input
                            type="number"
                            name="album-lanzamiento"
                            class="form-control form-control-user-custom"
                            placeholder="Año de lanzamiento"
                            min="500"
                            max="2030"
                            required="required">
                        <span class="help-block text-danger"><?php echo $album_lanzamiento_err; ?></span>
                    </div>
                    <!-- GÉNERO -->
                    <!-- <div class="col-4"> <input type="text" name="username" class="form-control
                    form-control-user" id="exampleInputEmail" aria-describedby="emailHelp"
                    placeholder="Género"> <span class="help-block text-danger"><?php echo
                    $username_err; ?></span> </div> -->
                    <!-- STOCK INICIAL -->
                    <div class="col-6">
                        <input
                            type="number"
                            name="album-stock"
                            class="form-control form-control-user-custom"
                            id="exampleInputEmail"
                            aria-describedby="Stock"
                            placeholder="Stock inicial"
                            min="0"
                            max="1000">
                        <span class="help-block text-danger"><?php echo $album_stock_err; ?></span>
                    </div>
                </div>
                <div
                    class="form-group row <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                    <!-- FORMATO-->
                    <div class="col-5">
                        <select
                            name="album-formato"
                            class="form-control form-control-user-custom"
                            required="required">
                            <option value="Elegir">Elegir Formato</option>
                        <?php // Cargar el primer Select
                                        $sql = "SELECT * FROM formats";
                                         if($result = mysqli_query($link, $sql)){
                                         if(mysqli_num_rows($result) > 0){
                                         while($row = mysqli_fetch_array($result)){
                                                 echo "<option value=".$row['id'].">".$row['format']."</option>";
                                         }
                                         } else{
                                          echo '<option>No hay formatos</option>';
                                          }
                                         } else { echo "Fatal Select."; }
                                        ?>
                        </select>
                        <span class="help-block text-danger"><?php echo $album_formato_err; ?></span>
                        <span class="help-block text-danger"><?php echo $format_ok_err; ?></span>
                    </div>
                    <!-- PRECIO DE DISCO -->
                    <div class="col-4">
                        <input
                            type="number"
                            name="album-precio"
                            class="form-control form-control-user-custom"
                            placeholder="Precio">
                        <span class="help-block text-danger"><?php echo $album_precio_err; ?></span>
                    </div>
                    <!-- IMAGEN DE DISCO -->
                    <div class="col-3">
                        <span class="help-block text-primary">Imagen:
                        </span>
                        <input type="file" name="album-img">
                        <span class="help-block text-danger"><?php echo $album_img_err; ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Añadir">
                    <a href="albums.php" class="btn btn-danger">Cancelar</a>
                </div>
                <!-- <a href="main.php" class="btn btn-primary btn-user btn-block"> Login </a>
                -->

            </form>

        </div>
        <!-- Content Row -->

        <?php include 'footer.php'; ?>