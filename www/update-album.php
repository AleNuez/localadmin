<?php include 'head.php'; ?>
<?php include 'navbar.php'; ?>
<?php include 'config.php'; ?>
<?php

 session_start();
 //0. Proceso la imagen

 if(isset($_POST['update'])){
    //path to store the uploaded img
    $target = "img/".basename($_FILES['image']['name']);
    $album_img = $_FILES['image']['name'];
} else {
    $album_img = "";
}

 //1. Definir variables y inicializarlas vacías.
$album_name = $album_artist = $album_year = $album_stock = $album_format = $album_price = $album_details = "";
$album_name_err = $album_artist_err = $album_year_err = $album_stock_err = $album_format_err = $album_price_err = $album_details_err = "";
 
// Processing form data when form is submitted
if(isset($_POST["id"]) && !empty($_POST["id"])){
    // Get hidden input value
    $id = $_POST["id"];

    // Validar nombre
    $input_name = trim($_POST["album-name"]);
    if(empty($input_name)){
        $album_name_err = "Por favor ingrese album.";
    } else{
        $album_name = $input_name;
    }    
    // Check input errors before updating in database
    if(empty($album_name_err)){
        // Prepare an update statement
        $sql = "UPDATE album SET name=?, artist_id=?, year=?, details=? WHERE id=?";
                
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "sissi", $param_name, $param_artist_id, $param_year, $param_details, $param_id);
            // Set parameters
            $param_name = $album_name;
            $param_artist_id = intval($_POST["album-artist"]);
            $param_year = $_POST["album-year"];
            $param_details = trim($_POST["album-details"]);
            $param_id = $_SESSION['album'];

            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Records updated successfully. Redirect to landing page
                
                ?>
                <!-- <script> location.replace("albums.php"); </script> -->
               <?php
                //header("location: index.php");
                //exit();
            } else{
                echo "Oops!Algo salió mal. Por favor, inténtelo de nuevo más tarde.";
            }
            
            
        }
        // Acá updateo articles
        $sql = "UPDATE articles SET article_id=?, album_id=?, format_id=?, price=?, stock=? WHERE article_id=?";
                
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "iiissi", $param_article_id, $param_album_id, $param_format_id, $param_price, $param_stock, $param_article_id);
            // Set parameters
            $param_article_id = $id;
            $param_album_id = $_SESSION['album'];
            $param_format_id = $_POST["album-format"];
            $param_price = $_POST['album-price'];
            $param_stock = $_POST['album-stock'];

            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Records updated successfully. Redirect to landing page
                
                ?>
                <script> location.replace("albums.php"); </script>
               <?php
                //header("location: index.php");
                exit();
            } else{
                echo "Oops!Algo salió mal. Por favor, inténtelo de nuevo más tarde.";
            }
            
            
        }
        // /Acá updateo articles


         // Close statement
        mysqli_stmt_close($stmt);
        

    }
    
    // Close connection
    mysqli_close($link);
} else{
    // Check existence of id parameter before processing further
    if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
        // Get URL parameter
        $id =  trim($_GET["id"]);
        
        // Esto recolecta info en Articles 
        $sql = "SELECT * FROM articles WHERE article_id = ?";
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "i", $param_id);
            // Set parameters
            $param_id = $id;
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                $result = mysqli_stmt_get_result($stmt);
                if(mysqli_num_rows($result) == 1){
                    /* Fetch result row as an associative array. Since the result set
                    contains only one row, we don't need to use while loop */
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    
                    // Retrieve individual field value
                    $article_id_place = $row["article_id"];
                    $article_album_id_place = $row["album_id"];
                    $_SESSION['album'] = $article_album_id_place;
                    $article_format_id_place = $row["format_id"];
                    $article_price_place = $row["price"];
                    $article_stock = strval($row["stock"]);
                } else{
                    // URL doesn't contain valid id. Redirect to error page
                    ?>
                    <script> location.replace("albums.php"); </script>
                   <?php
                    //header("location: error.php");
                    exit;
                }
                
            } else{
                echo "Oops! Algo salió mal. Por favor, inténtelo de nuevo más tarde.";
            }
        }
        // /Esto recolecta info en Articles

        // Busco data de Albumes para rellenar los campos
        $sql = "SELECT * FROM album WHERE id = ?";
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "i", $param_id);
            // Set parameters
            $param_id = $article_album_id_place;
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                $result = mysqli_stmt_get_result($stmt);
                if(mysqli_num_rows($result) == 1){
                    /* Fetch result row as an associative array. Since the result set
                    contains only one row, we don't need to use while loop */
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    
                    // Retrieve individual field value
                    $album_id_place = $row["id"];
                    $album_name_place = $row["name"];
                    $album_artist_place = $row["artist_id"];
                    $album_year_place = $row["year"];
                    $album_details_place = trim($row["details"]);
                } else{
                    // URL doesn't contain valid id. Redirect to error page
                    ?>
                    <script> location.replace("albums.php"); </script>
                   <?php
                    //header("location: error.php");
                    exit;
                }
                
            } else{
                echo "Oops! Algo salió mal. Por favor, inténtelo de nuevo más tarde.";
            }
        }
        //mysqli_stmt_close($stmt);

        
        // Close statement
       // mysqli_stmt_close($stmt);
        
        // Close connection
        //mysqli_close($link);
    }  else{
        // URL doesn't contain id parameter. Redirect to error page
        
        ?>
                 <script> location.replace("artists.php"); </script>
                <?php
        //header("location: error.php");
        exit();
    }
}
// Este método mueve el archivo temporal a la carpeta de mi repositorio pero al parecer no tengo permisos y la acción no se ejecuta
// https://stackoverflow.com/questions/8103860/move-uploaded-file-gives-failed-to-open-stream-permission-denied-error

// if(move_uploaded_file($_FILES['image']['tmp_name'], 'img/')){
// $msj= "ok";
// } else {
//     $msj ="no ok";
// }
?>
<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Editar album del catálogo</h1>
<!-- Content Row -->
<div class="row">
<div class=" col-12 card shadow mb-4">
        <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Modificar Disco</h6>
        </div>    
        <div class="card-body">
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>"method="post" class="user" enctype="multipart/form-data">
            <p class="text-primary">Modificar Album en general</p>    
            <div class="form-group row">
                        <!-- NOMBRE DE ALBUM -->
                        <div class="col-4">
                                            <input
                                                type="text"
                                                name="album-name"
                                                class="form-control form-control-user-custom"
                                                id="album-name"
                                                placeholder="Nombre"
                                                value="<?php echo $album_name_place;?>"
                                                required="required">
                                            <span class="help-block text-danger"><?php echo $album_nombre_err; ?></span>
                        </div>  
                        <!-- ARTISTA -->
                        <div class="col-4">
                                            <select name="album-artist" class="form-control form-control-user-custom" required>
                                            <?php // Cargar el primer Select
                                            $sqli = "SELECT * FROM artist WHERE id=$album_artist_place";
                                            if($resulti = mysqli_query($link, $sqli)){
                                            if(mysqli_num_rows($resulti) > 0){
                                            while($rowi = mysqli_fetch_array($resulti)){
                                                    echo "<option value=".$rowi['id'].">".$rowi['name']."</option>";
                                            }
                                            } else{
                                            echo '<option>No hay artista</option>';
                                            }
                                            } else { echo "Fatal Select."; }
                                            ?>
                                            <?php // Cargar el resto del Select
                                        $sqli = "SELECT * FROM artist";
                                         if($resulti = mysqli_query($link, $sqli)){
                                         if(mysqli_num_rows($resulti) > 0){
                                         while($rowi = mysqli_fetch_array($resulti)){
                                                 echo "<option value=".$rowi['id'].">".$rowi['name']."</option>";
                                         }
                                         } else{
                                          echo '<option>No hay géneros</option>';
                                          }
                                         } else { echo "Fatal Select."; }
                                        ?>
                                            </select>
                                            <span class="help-block text-danger"><?php echo $artist_err; ?></span>
                        </div>   
                        <!-- LANZAMIENTO -->
                        <div class="col-4">
                                            <input
                                                type="number"
                                                name="album-year"
                                                class="form-control form-control-user-custom"
                                                placeholder="Año de lanzamiento"
                                                min="500"
                                                max="2030"
                                                value="<?php echo $album_year_place;?>"
                                                required="required">
                                            <span class="help-block text-danger"><?php echo $album_lanzamiento_err; ?></span>
                        </div>
                </div>
                <div class="form-group row"> 
                    <br> 
                        <!-- DETALLES -->
                        <div class="col-12">
                                          
                                            <?php //echo var_dump($details_place); ?>
                                            <textarea name="album-details" class="form-control" rows="5" required><?php echo (!empty($album_details_place)) ? $album_details_place : 'Acerca de...';?></textarea>
                                            <span class="help-block text-danger"><?php echo $details_err; ?></span>
                        </div>        
                </div> 
                <hr class="sidebar-divider my-0">
                <br>
                <p class="text-primary">Modificar Artículo</p> 
                <div class="form-group row <?php echo (!empty($details_err)) ? 'has-error' : ''; ?>">
                <!-- STOCK -->
                <div class="col-4">
                <span class="help-block text-secondary">Ejemplares:</span><br>
                                            <input
                                                type="number"
                                                name="album-stock"
                                                class="form-control form-control-user-custom"
                                                id="exampleInputEmail"
                                                aria-describedby="Stock"
                                                placeholder="Stock inicial"
                                                min="0"
                                                max="1000"
                                                value="<?php echo $article_stock;?>">
                                            <span class="help-block text-danger"><?php echo $album_stock_err; ?></span>
                </div>        
                <!-- FORMATO -->
                        <div class="col-4">
                        <span class="help-block text-secondary">Formato:</span><br>
                                            <select
                                                name="album-format"
                                                class="form-control form-control-user-custom"
                                                required="required">
                                                <!-- <option value="Elegir">Elegir Formato</option> -->
                                                <?php // Cargar el primer Select
                                        $sqli = "SELECT * FROM formats WHERE id=$article_format_id_place";
                                         if($resulti = mysqli_query($link, $sqli)){
                                         if(mysqli_num_rows($resulti) > 0){
                                         while($rowi = mysqli_fetch_array($resulti)){
                                                 echo "<option value=".$rowi['id'].">".$rowi['format']."</option>";
                                         }
                                         } else{
                                          echo '<option>No hay géneros</option>';
                                          }
                                         } else { echo "Fatal Select."; }
                                        ?>
                                        
                                        <!-- <?php // Cargar el resto del Select
                                        // $sqli = "SELECT * FROM formats";
                                        //  if($resulti = mysqli_query($link, $sqli)){
                                        //  if(mysqli_num_rows($resulti) > 0){
                                        //  while($rowi = mysqli_fetch_array($resulti)){
                                        //          echo "<option value=".$rowi['id'].">".$rowi['format']."</option>";
                                        //  }
                                        //  } else{
                                        //   echo '<option>No hay géneros</option>';
                                        //   }
                                        //  } else { echo "Fatal Select."; }
                                        ?> -->
                                            </select>
                                            <span class="help-block text-danger"><?php echo $album_formato_err; ?></span>
                                            <span class="help-block text-danger"><?php echo $format_ok_err; ?></span>
                        </div>
                        <!-- PRECIO DE DISCO -->
                        <div class="col-4">
                        <span class="help-block text-secondary">Precio:</span><br>
                                            <input
                                                type="number"
                                                name="album-price"
                                                class="form-control form-control-user-custom"
                                                placeholder="Precio"
                                                value="<?php echo $article_price_place;?>"
                                                >
                                            <span class="help-block text-danger"><?php echo $album_precio_err; ?></span>
                        </div>
                        <!-- IMAGEN DE DISCO -->
                        <!-- <div class="col-3">
                                            <span class="help-block text-primary">Imagen:
                                            </span>
                                            <input type="file" name="album-img">
                                            <span class="help-block text-danger"><?php echo $album_img_err; ?></span>
                        </div> -->
                </div>
                   
                <div class="form-group row">
                    <br>
                        <!-- ENVIAR -->
                                         <input type="hidden" name="id" value="<?php echo $id;?>"/>
                                         <input type="submit" name="update" class="btn btn-primary" value="Actualizar">
                                         <a href="albums.php" class="btn btn-danger">Cancelar</a>
                </div>                                     
            </form>
        </div>
<!-- Content Row -->
<?php include 'footer.php'; ?>