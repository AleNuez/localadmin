<?php include 'head.php'; ?>
<?php include 'navbar.php'; ?>

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Ultimas Ventas</h1>
<!-- Content Row -->
<div class="row">

    <!-- Content Row -->
    <!-- DataTales Example -->
    <div class=" col-12 card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Ventas</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="tabla" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Artista</th>
                            <th>Año</th>
                            <th>Formato</th>
                            <th>Precio</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr>
                            <td>A001</td>
                            <td>Toxicity</td>
                            <td>System Of a Down</td>
                            <td>2001</td>
                            <td>CD</td>
                            <td>$3000</td>
                            <td>Ver Más</td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- </div> -->
    <!--/ DataTales Example -->
    <!-- Bar Chart -->
    <div class="col-8 card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Discos Vendidos</h6>
        </div>
        <div class="card-body">
            <div class="chart-bar">
                <canvas id="myBarChart"></canvas>
            </div>
            <hr>
            Cantidad de productos vendidos en los ultimos 6 meses
        </div>
    </div>

    <!-- /chart -->
    <?php include 'footer.php'; ?>