-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Jul 28, 2022 at 04:36 AM
-- Server version: 5.5.62
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `LocalAdmin`
--

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `artist_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  `cover` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `year` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `publisher_id` int(11) NOT NULL,
  `details` varchar(600) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id`, `name`, `artist_id`, `genre_id`, `cover`, `year`, `publisher_id`, `details`) VALUES
(7, 'Discovery', 13, 0, 'https://f4.bcbits.com/img/a4139357031_10.jpg', '2001', 0, 'Discovery es el segundo álbum de estudio del dúo francés de música house Daft Punk, lanzado en marzo de 2001. Marcó un cambio en el sonido desde el Chicago house, género por el que eran conocidos, al disco, post-disco y house inspirado en el synthpop.​'),
(8, 'Future Nostalgia', 14, 0, 'https://f4.bcbits.com/img/a4139357031_10.jpg', '2020', 0, 'Future Nostalgia es el segundo álbum de estudio de la cantante británica Dua Lipa.​​ Fue lanzado el 27 de marzo de 2020 por el sello discográfico Warner Records.​');

--
-- Triggers `album`
--
DELIMITER $$
CREATE TRIGGER `TRIGGER_CREATE_ALBUM_HISTORIC` AFTER INSERT ON `album` FOR EACH ROW INSERT into historic_names (id,name) VALUES (NEW.id,NEW.name)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `article_id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `format_id` int(11) NOT NULL,
  `price` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `stock` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`article_id`, `album_id`, `format_id`, `price`, `stock`) VALUES
(9, 7, 2, '1500', 1),
(10, 7, 1, '3000', 1),
(11, 8, 2, '3000', 2);

--
-- Triggers `articles`
--
DELIMITER $$
CREATE TRIGGER `TRIGGER_CREATE_ALBUM` AFTER INSERT ON `articles` FOR EACH ROW INSERT into audits (date,action,article,format) VALUES (CURRENT_TIMESTAMP(),"Se agregó disco ↑",NEW.album_id,NEW.format_id)
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `TRIGGER_DELETE_ALBUM` AFTER DELETE ON `articles` FOR EACH ROW INSERT into audits (date,action,article,format) VALUES (CURRENT_TIMESTAMP(),"Se borró disco 	↓",OLD.album_id,OLD.format_id)
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `TRIGGER_UPDATE_ALBUM` AFTER UPDATE ON `articles` FOR EACH ROW INSERT into audits
(date,action,article,format) VALUES (CURRENT_TIMESTAMP(),"Se modificó disco ↻",NEW.album_id,NEW.format_id)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `artist`
--

CREATE TABLE `artist` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `genre_id` int(11) NOT NULL,
  `country` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `image` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `details` varchar(600) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `artist`
--

INSERT INTO `artist` (`id`, `name`, `genre_id`, `country`, `image`, `details`) VALUES
(13, 'Daft Punk', 8, 'Francia', '', 'Daft Punk fue un dúo francés de french house formado por los DJ\'s y productores Guy-Manuel de Homem-Christo y Thomas Bangalter.​​​'),
(14, 'Dua Lipa', 10, 'Reino Unido', '286374516_563134645297469_6724622113099156237_n.jpg', 'Dua Lipa (Londres, 22 de agosto de 1995) es una cantante, compositora, modelo y actriz británica de origen albanokosovar.');

-- --------------------------------------------------------

--
-- Table structure for table `audits`
--

CREATE TABLE `audits` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `action` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `article` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `artist` int(11) NOT NULL,
  `format` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `audits`
--

INSERT INTO `audits` (`id`, `date`, `action`, `article`, `artist`, `format`) VALUES
(1, '2022-07-26 06:12:04', 'Se agregó disco ↑', '7', 0, 2),
(2, '2022-07-26 06:12:11', 'Se modificó disco ↻', '7', 0, 2),
(3, '2022-07-26 06:16:23', 'Se agregó disco ↑', '7', 0, 1),
(4, '2022-07-26 06:19:26', 'Se agregó disco ↑', '8', 0, 2),
(5, '2022-07-26 06:19:58', 'Se modificó disco ↻', '8', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `formats`
--

CREATE TABLE `formats` (
  `id` int(11) NOT NULL,
  `format` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `formats`
--

INSERT INTO `formats` (`id`, `format`) VALUES
(1, 'Vinilo'),
(2, 'CD'),
(3, 'Cassette');

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

CREATE TABLE `genre` (
  `id` int(11) NOT NULL,
  `genre` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`id`, `genre`) VALUES
(8, 'Electronica'),
(9, 'Rock'),
(10, 'Pop');

-- --------------------------------------------------------

--
-- Table structure for table `historic_names`
--

CREATE TABLE `historic_names` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `historic_names`
--

INSERT INTO `historic_names` (`id`, `name`) VALUES
(19, 'Parachutes'),
(20, 'Viva la Vida'),
(21, 'The White Album'),
(1, 'Master Of Puppets'),
(2, 'Parachutes'),
(3, 'And Justice For All...'),
(4, 'The Red Hot Chili Peppers'),
(5, 'Freakey Styley'),
(6, 'The Uplift Mofo Party Plan'),
(7, 'Discovery'),
(8, 'Future Nostalgia');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `album_id` int(11) NOT NULL,
  `album_stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `publishers`
--

CREATE TABLE `publishers` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `publishers`
--

INSERT INTO `publishers` (`id`, `name`, `email`, `phone`) VALUES
(1, 'EMI', '', ''),
(2, 'Warner Music', '', ''),
(3, 'Universal', '', ''),
(4, 'Apple Records', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'no_image.jpg',
  `active` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `last_name`, `password`, `email`, `picture`, `active`, `rol_id`) VALUES
(1, 'admin', 'Ricardo', 'Gogolewski', '$2y$10$nvWxQPKVXEPomZYDM1YgaeQbJQze2WExePpUyuXN8pTipzCRhJP/a', 'admin@admin.com', '[value-6]', 0, 1),
(2, 'visitor', 'Alejandro', 'Nuñez', '$2y$10$2NrtpxN8v5cxOb8R2HVciu9gW3tcrac43RktQYBvMzPNyZsyGPFyi', 'visitor@visitor.com', '[value-6]', 0, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`article_id`);

--
-- Indexes for table `artist`
--
ALTER TABLE `artist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `audits`
--
ALTER TABLE `audits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `formats`
--
ALTER TABLE `formats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD KEY `album_id` (`album_id`);

--
-- Indexes for table `publishers`
--
ALTER TABLE `publishers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `album_id` (`album_id`,`customer_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rol_id` (`rol_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `artist`
--
ALTER TABLE `artist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `audits`
--
ALTER TABLE `audits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `formats`
--
ALTER TABLE `formats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `genre`
--
ALTER TABLE `genre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `publishers`
--
ALTER TABLE `publishers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `inventory`
--
ALTER TABLE `inventory`
  ADD CONSTRAINT `inventory_ibfk_1` FOREIGN KEY (`album_id`) REFERENCES `albums` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
