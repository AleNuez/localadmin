<?php include 'head.php'; ?>
<?php include 'navbar.php'; ?>
<?php include 'config.php'; ?>
<?php
 
// Define variables and initialize with empty values
$genre = "";
$genre_err = "";
 
// Processing form data when form is submitted
if(isset($_POST["id"]) && !empty($_POST["id"])){
    // Get hidden input value
    $id = $_POST["id"];
    
    // Validar nombre
    $input_genre = trim($_POST["genre-nombre"]);
    if(empty($input_genre)){
        $genre_err = "Por favor ingrese género.";
    } elseif(!filter_var($input_genre, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $genre_err = "Por favor ingrese un Nombre válido";
    } else{
        $genre = $input_genre;
    }    
    // Check input errors before updating in database
    if(empty($genre_err)){
        // Prepare an update statement
        $sql = "UPDATE genre SET genre=? WHERE id=?";
                
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "si", $param_genre, $param_id);
            // Set parameters
            $param_genre = $genre;
            $param_id = $id;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Records updated successfully. Redirect to landing page
                ?>
                <script> location.replace("genres.php"); </script>
               <?php
                //header("location: index.php");
                exit();
            } else{
                echo "Oops!Algo salió mal. Por favor, inténtelo de nuevo más tarde.";
            }
            
        }
           
         // Close statement
        mysqli_stmt_close($stmt);
        
    }
    
    // Close connection
    mysqli_close($link);
} else{
    // Check existence of id parameter before processing further
    if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
        // Get URL parameter
        $id =  trim($_GET["id"]);
        
        // Prepare a select statement
        $sql = "SELECT * FROM genre WHERE id = ?";
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "i", $param_id);
            
            // Set parameters
            $param_id = $id;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                $result = mysqli_stmt_get_result($stmt);
    
                if(mysqli_num_rows($result) == 1){
                    /* Fetch result row as an associative array. Since the result set
                    contains only one row, we don't need to use while loop */
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    
                    // Retrieve individual field value
                    $genre = $row["genre"];
                } else{
                    // URL doesn't contain valid id. Redirect to error page
                    ?>
                    <script> location.replace("genres.php"); </script>
                   <?php
                    //header("location: error.php");
                    exit;
                }
                
            } else{
                echo "Oops! Algo salió mal. Por favor, inténtelo de nuevo más tarde.";
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
        
        // Close connection
        mysqli_close($link);
    }  else{
        // URL doesn't contain id parameter. Redirect to error page
        ?>
                 <script> location.replace("genres.php"); </script>
                <?php
        //header("location: error.php");
        exit();
    }
}
?>
<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Editar un género musical al catálogo</h1>
<!-- Content Row -->
<div class="row">
<div class=" col-12 card shadow mb-4">
        <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Modificar género</h6>
        </div>    
        <div class="card-body">
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>"method="post" class="user">
                                        <div class="form-group row <?php echo (!empty($genre_err)) ? 'has-error' : ''; ?>">
                                        <!-- NOMBRE DEL GÉNERO -->
                                        <div class="col-12">
                                        <input type="text" name="genre-nombre" class="form-control form-control-user"
                                                id="genre-nombre" aria-describedby="genre-nombre"
                                                placeholder="<?php echo $genre;?>" value="<?php echo $genre;?>" required>
                                                <span class="help-block text-danger"><?php echo $genre_err; ?></span>
                                        </div>    
                                        <input type="hidden" name="id" value="<?php echo $id;?>"/>
                                        </div>

                                        <div class="form-group">
                                         <input type="submit" class="btn btn-primary" value="Actualizar">
                                         <a href="genres.php" class="btn btn-danger">Cancelar</a>
                                        </div>
                                        <!-- <a href="main.php" class="btn btn-primary btn-user btn-block">
                                            Login
                                        </a> -->
                                       
                                    </form>

        </div>
<!-- Content Row -->
<?php include 'footer.php'; ?>