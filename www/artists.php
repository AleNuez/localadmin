<?php include 'head.php'; ?>
<?php include 'navbar.php'; ?>
<?php include 'config.php'; ?>
<?php session_start(); ?>
<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Artistas</h1>
<!-- Content Row -->
<div class="row">

    <!-- Content Row -->
    <!-- DataTales Example -->
    <div class="col-7 card shadow m-2">
        <div class="card-header py-3">
            <div class="row">
                <div class="col-8">
                    <h6 class="m-0 font-weight-bold text-primary">Intérpretes | La Música de Gogo</h6> 
                </div>
                <div class="col-4"> 
                <?php if($_SESSION["rol_id"] == "1") { ?>
                    <form action="add-artist.php" method="post">
                    <input type="submit" class="btn btn-primary" value=" + | Agregar Intérprete">
                    </form>
                    <?php } ?>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
            <?php  $sql = "SELECT artist.id as ID, artist.name as Artista, genre.genre as Género, country as Origen FROM artist INNER JOIN genre ON artist.genre_id = genre.id;"; ?>
                <table class="table table-bordered" id="tabla" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Artista</th>
                            <th>Género</th>
                            <th>Orígen</th>
                            <?php if($_SESSION["rol_id"] == "1") { ?><th>Acciones</th>  <?php } ?>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody>
                    <?php   if($result_busq = mysqli_query($link, $sql)){
                        if(mysqli_num_rows($result_busq) > 0){
                            while($row_busq = mysqli_fetch_array($result_busq)){
                                echo "<tr>";
                                    echo "<td>" . $row_busq['ID'] . "</td>";
                                    echo "<td>" . $row_busq['Artista'] . "</td>";
                                    echo "<td>" . $row_busq['Género'] . "</td>";
                                    echo "<td>" . $row_busq['Origen'] . "</td>";
                                    if($_SESSION["rol_id"] == "1") { 
                                    echo "<td>";
                                    $sql2 = "SELECT COUNT(artist_id) as Disponible FROM album WHERE artist_id ={$row_busq['ID']};";
                                    if($result_busq2 = mysqli_query($link, $sql2)){
                                          if(mysqli_num_rows($result_busq2) > 0){
                                              while($row_busq2 = mysqli_fetch_array($result_busq2)){
                                                //$disponible[] = $row_busq2['Disponible'];
                                                $disponibles = $row_busq2['Disponible'];}}}
                                    // echo '<a href="read.php?id='.$row_busq2['genre_id'] .'" class="mr-3" title="Ver" data-toggle="tooltip"><span class="fa fa-eye"></span></a>';
                                    // Si hay artistas con albumes guardados imposibilito el poder eliminar el artista, solo se pueden borrar los artistas sin albumes relacionados
                                    $disabled_click = ($disponibles>0) ? "style=\"pointer-events:none\";" : ""; 
                                    $disabled_color = ($disponibles>0) ? "text-secondary" : "text-danger"; 
                                    echo '<a href="update-artist.php?id='.$row_busq['ID'] .'" class="mr-3" title="Editar" data-toggle="tooltip"><span class="fa fa-edit text-success"></span></a>';
                                     echo '<a '.$disabled_click.' href="delete-artist.php?id='.$row_busq['ID'] .'" title="Eliminar" data-toggle="tooltip"><span class="fa fa-trash '.$disabled_color.'"></span></a>';
                                     echo "</td>";
                                              }
                                echo "</tr>";
                            }
                        }
                        }
                            ?>
                        <!-- <tr>
                            <td>I001</td>
                            <td>System Of a Down</td>
                            <td>Metal</td>
                            <td>CD, Vinilo</td>
                            <td>Ver Más</td>
                        </tr> -->

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- </div> -->
    <!--/ DataTables -->

    <!-- Illustrations -->

    <div class=" col-4 card shadow m-2">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Artista del Día</h6>
        </div>
        <div class="card-body">
        <?php  $sql_b = "SELECT artist.name as Artista, image as Imagen, details as Acerca FROM artist ORDER BY RAND() LIMIT 1;"; ?>
            <div class="text-center">
            <?php   if($result_busq_b = mysqli_query($link, $sql_b)){
                        if(mysqli_num_rows($result_busq_b) > 0){
                            while($row_busq_b = mysqli_fetch_array($result_busq_b)){
                                    $dayimg = $row_busq_b['Imagen'];
                                    $dayname = $row_busq_b['Artista'];
                                    $daydata = $row_busq_b['Acerca'];
                            }}}?>
              
              <?php 
              // DESHABILITADO HASTA QUE SE PUEDAN SUBIR IMAGENES 
              //echo var_dump(base64_encode($dayimg)); ?>
                    <!-- <img style="width:100px;height:100px;" src='data:image/jpg;base64,<?php //echo base64_encode($dayimg);?>'/> -->
            <img style="width:200px;height:200px;" src="https://i.pinimg.com/474x/1e/1e/49/1e1e4996b0f17197b81e578450462c14.jpg" alt="">
                </div>
            <h6><b><?php $dayname = isset($dayname) ? $dayname : "Banda del Día"; echo $dayname; ?></b></h6>
            <p><?php $daydate = isset($daydata) ? $daydata : "Información esencial."; echo $daydata; ?></p>
            <a target="_blank" rel="nofollow" href="https://www.google.com/search?q=<?php $dayname = isset($dayname) ? $dayname : "Banda del Día"; echo $dayname; ?>">Conocer más sobre el artista &rarr;</a>
        </div>
    </div>

    <!-- /ilustrations -->

    <?php include 'footer.php'; ?>