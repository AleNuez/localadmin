<?php include 'head.php'; ?>
<?php include 'navbar.php'; ?>
<?php include 'config.php'; ?>
<?php
// Process delete operation after confirmation
if(isset($_POST["id"]) && !empty($_POST["id"])){    
    // Prepare a delete statement
    $sql = "DELETE FROM genre WHERE id = ?";
    
    if($stmt = mysqli_prepare($link, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "i", $param_id);
        
        // Set parameters
        $param_id = trim($_POST["id"]);
        
        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            // Records deleted successfully. Redirect to landing page
            ?>
            <script> location.replace("genres.php"); </script>
           <?php
           // header("location: genres.php");
            exit();
        } else{
            echo "Oops! Algo salió mal. Por favor, inténtelo de nuevo más tarde.";
        }
    }
     
    // Close statement
    mysqli_stmt_close($stmt);
    
    // Close connection
    mysqli_close($link);
} else{
    // Check existence of id parameter
    if(empty(trim($_GET["id"]))){
        // URL doesn't contain id parameter. Redirect to error page
        ?>
        <script> location.replace("genres.php"); </script>
       <?php
        //header("location: genres.php");
        exit();
    }
}
?>
<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Eliminar</h1>
<!-- Content Row -->
<div class="row">
<div class=" col-12 card shadow mb-4">
        <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Borrar Registro de Datos</h6>
        </div>    
        <div class="card-body">
    
        <div class="row">
                <div class="col-md-12">
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="alert alert-danger">
                            <input type="hidden" name="id" value="<?php echo trim($_GET["id"]); ?>"/>
                            <p>Esta completamente seguro que desea eliminar el Registro?</p>
                            <p>
                                <input type="submit" value="Si" class="btn btn-danger">
                                <a href="genres.php" class="btn btn-secondary">No</a>
                            </p>
                        </div>
                    </form>
                </div>
            </div>     
        </div>
<!-- Content Row -->
<?php include 'footer.php'; ?>

       
 